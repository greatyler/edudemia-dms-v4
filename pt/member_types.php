<?php








// Register Custom Post Type
function edudms_pt_member_type_post_type() {

	$labels = array(
		'name'                  => _x( 'Member Types', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Member Type', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Member Types', 'text_domain' ),
		'name_admin_bar'        => __( 'Member Type', 'text_domain' ),
		'archives'              => __( 'Member Type Archives', 'text_domain' ),
		'parent_field_colon'     => __( 'Parent Member Type:', 'text_domain' ),
		'all_fields'             => __( 'All Member Types', 'text_domain' ),
		'add_new_field'          => __( 'Add New Field', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_field'              => __( 'New Member Type', 'text_domain' ),
		'edit_field'             => __( 'Edit Member Type', 'text_domain' ),
		'update_field'           => __( 'Update Member Type', 'text_domain' ),
		'view_field'             => __( 'View Member Type', 'text_domain' ),
		'search_fields'          => __( 'Search Member Type', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_field'      => __( 'Insert into Member Type', 'text_domain' ),
		'uploaded_to_this_field' => __( 'Uploaded to this Member Type', 'text_domain' ),
		'fields_list'            => __( 'Fields list', 'text_domain' ),
		'fields_list_navigation' => __( 'Fields list navigation', 'text_domain' ),
		'filter_fields_list'     => __( 'Filter fields list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Member Type', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'page-attributes', 'revisions' ),
		'taxonomies'            => array( ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => 'edudms_ptslug',
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'edudms_cap',
		'map_meta_cap'        => true
	);
	register_post_type( 'member_type', $args );

}
add_action( 'init', 'edudms_pt_member_type_post_type', 0 );



























?>