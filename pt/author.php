<?php get_header(); ?>

<div id="content">

<!-- This sets the $curauth variable -->

    <?php
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    ?>

	<?php
	//print_r($curauth);
	$edudms_pt_user = get_user_meta($curauth->data->ID);
	$edudms_pt_user_base_data = get_user_by("ID", $curauth->data->ID);
	$edudms_pt_author_ID = $curauth->data->ID;
	
	//establish and populate edudms_fields_data_array
	$edudms_fields_data_array = array();
	
	//$all_the_fields = get_posts(array('post_type' => 'pt_field'));
	$edudms_field_list = edudms_pt_generate_edudms_field_list();
	
	
	foreach($edudms_field_list as $fieldlet) {
		
		
		
		//$field_name = 
		$fieldlet_data = get_field('edudms_tab_' . $fieldlet["edudms_field_option_slug"], "user_" . $curauth->data->ID);
		
		//$fieldlet_data = $edudms_pt_user['edudms_tab_' . $fieldlet["edudms_field_ID"]][0];

		if(!empty($fieldlet_data)) {
		$edudms_fields_data_array[$fieldlet["edudms_field_ID"]] = $fieldlet_data;
		}	
		
	}
	
	
	
	//print_r($userstuff);
	?>
	
	<div class="edudms-pt-profile-wrapper">
		<div class="edudms-pt-profile-block-picture">
			<img src="<?php echo get_avatar_url( $edudms_pt_user_base_data->user_email ); ?>" alt="Profile Picture" class="edudms-pt-profile-pic author-page">
		</div>
		<div class="edudms-pt-profile-block-title">
			<?php echo $edudms_pt_user["last_name"][0]; ?> <?php echo $edudms_pt_user["first_name"][0]; ?>
			<?php echo $edudms_pt_user["edudms_title"][0]; ?>
			<?php echo $curauth->data->ID ?>
		</div>
		<div class="edudms-pt-profile-block-one">
			<?php echo $edudms_pt_user["edudms_title"][0]; ?><br>
			Phone: <?php echo $edudms_pt_user["edudms_phone"][0]; ?><br>
			Office: <?php echo $edudms_pt_user["office"][0]; ?><br>
			Email: <a href="mailto:<?php echo $edudms_pt_user_base_data->user_email; ?>"><?php echo $edudms_pt_user_base_data->user_email; ?></a><br>
			<?php if(!empty($edudms_pt_user_base_data->user_url)) { ?>
			Website: <a href="<?php echo $edudms_pt_user_base_data->user_url; ?>"><?php echo $edudms_pt_user_base_data->user_url; ?></a>
			<?php } ?>
		</div>	
		<div class="edudms-pt-profile-block-two">
			<?php edudms_pt_output_tabs_on_author_profile($edudms_fields_data_array, $edudms_field_list); ?>
			<?php //print_r($edudms_pt_user); ?>
			<?php //print_r($edudms_pt_user_base_data); ?>
		</div>
		<div class="edudms-pt-profile-block-three">
			
		</div>
		
	</div>
	
	<?php //print_r($edudms_pt_user); ?>
	
	
	
	
	
	
	
	
	
	
	
	
	

</div>


















<?php

	
function edudms_pt_output_tabs_on_author_profile($edudms_fields_data_array, $edudms_field_list) {
	
	//print_r($edudms_fields_data_array);
	if(!empty($edudms_fields_data_array)) {
	foreach($edudms_fields_data_array as $tabbette_key => $tabbette_value) {
	
		foreach($edudms_field_list as $key) {
			
			if($tabbette_key == $key["edudms_field_ID"]) {
				echo do_shortcode('[tabby title="' . $key["edudms_field_option_proper_name"] . '"]');
			}
			
		}
		echo $tabbette_value;
	}
	echo do_shortcode('[tabbyending]');
	}
	
}
?>

















































































<?php get_sidebar(); ?>
<?php get_footer(); ?>