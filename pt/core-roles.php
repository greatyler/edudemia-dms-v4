<?php





add_role('faculty', __(
   'Faculty'),
   array(
       'read'            => true, // Allows a user to read
       'create_posts'      => true, // Allows user to create new posts
       )
);





add_action('admin_init','edudms_add_role_caps',999);
function edudms_add_role_caps() {
 
 // Add the roles you'd like to administer the custom post types
$roles = array('editor','administrator');
 
 // Loop through each role and assign capabilities
foreach($roles as $the_role) { 
 
     $role = get_role($the_role);
 
            $role->add_cap( 'read' );
			$role->add_cap( 'read_edudms_cap');
			$role->add_cap( 'read_private_edudms_caps' );
			$role->add_cap( 'edit_edudms_cap' );
			$role->add_cap( 'edit_edudms_caps' );
			$role->add_cap( 'edit_others_edudms_caps' );
			$role->add_cap( 'edit_published_edudms_caps' );
			$role->add_cap( 'publish_edudms_caps' );
			$role->add_cap( 'delete_others_edudms_caps' );
			$role->add_cap( 'delete_private_edudms_caps' );
			$role->add_cap( 'delete_published_edudms_caps' );
 
}
 
}






	
	
	




?>
