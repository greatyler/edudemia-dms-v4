<?php




//shortcode for showing a list of people
add_shortcode ('pt_list', 'edudms_pt_show_people_list');
add_shortcode ('people', 'edudms_pt_show_people_list');

function edudms_pt_show_people_list($atts) {
	
	ob_start();
	//start people-wrapper outside list or tile	?>	
	<p>
	<div class="people-wrapper">
	<?php
	
	$shortcode_atts = shortcode_atts( array(
        'member_type' 	=> 'faculty',
		'sort_by' 		=> 'last_name',
		'meta_key' 		=> 'edudms_pt_member_type',
		'meta_value' 	=> 'faculty',
		'format' 		=> 'list',
		'fields' 		=> 'full_name,title,email,phone,office',
		'headers' 		=> 'Name,Title,Email,Phone,Office',
		'field'			=> 'unknown',
		'style'			=> 'simple'
    ), $atts );
	

	
	
	if ($shortcode_atts['format'] == 'tile') {
		
		
		
	}

	
	
	if ($shortcode_atts['format'] == 'list') {
		
		
		
		
		$edudms_list_users = edudms_pt_filter_by_acf_field('member_type', 'faculty');
		
		
		//print_r($edudms_list_users);
		
		foreach ($edudms_list_users as $user) {
			edudms_pt_output_person_list_block($user);
		}
		
		
	}
	
	//end people-wrapper outside list or tile
	?></div>
	</p>
	<?php
	
	if ($shortcode_atts['format'] == 'table') {
		
		
	}
	
	if ($shortcode_atts['format'] == 'card') {
		
		
	}
	
	
	
	$buffer_stuff = ob_get_clean();
	
	return $buffer_stuff;
}




/**************************************
People List Functions
***************************************/


function edudms_get_people_for_list($member_type = 'faculty', $sort_by = 'first_name', $field = 'default') {
	
	$args = array(
		'blog_id'      => $GLOBALS['blog_id'],
		'role'         => '',
		'meta_key'     => 'edudms_pt_member_type',
		'meta_value'   => $member_type,
		'meta_compare' => '',
		'meta_query'   => array(),
		'date_query'   => array(),        
		'include'      => array(),
		'exclude'      => array(),
		'offset'       => '',
		'search'       => '',
		'number'       => '',
		'count_total'  => false,
		'fields'       => 'all',
		'who'          => '',
	); 

	$some_users = get_users( $args );
}

function edudms_find_member_type($title) {
	$edudms_all_member_types = get_posts(array('post_type' => 'member_type'));
	foreach($edudms_all_member_types as $type) {
		if($type->post_title == $title) {
			return $type;
		}
	}
}

function edudms_pt_filter_by_acf_field( $field = "nope", $value = "nope", $get_users = "yes", $users = "") {
	if($get_users == "yes") {$all_users = get_users();}
	
	$filtered_users = array();
	//print_r($all_users);
	
	if($field == "member_type") {
		$member_type_object = edudms_find_member_type($value);
		$value = $member_type_object->ID;
		$truefalse = false;}

	foreach($all_users as $single_user) {
		$that_field = get_field($field, 'user_' . $single_user->ID, $truefalse);
		if($that_field == $value) {
			array_push($filtered_users, $single_user);
			
		}
	}
	
	return $filtered_users;
	//print_r($filtered_users);
	
	}
	
	
function edudms_pt_output_person_list_block($user) {
	$edudms_author_meta = get_user_meta($user->ID);
	$edudms_pt_site_url = get_site_url();
	
	?>
	<div class="edudms-pt-list-block-wrapper <?php echo $edudms_author_meta["last_name"][0]; ?>-<?php echo $edudms_author_meta["first_name"][0]; ?>">
		<div class="edudms-pt-list-first-line-wrapper">
			<div class="pt-name">
				<a href="<?php echo $edudms_pt_site_url; ?>/people/<?php echo $edudms_author_meta["nickname"][0]; ?>"><?php echo $edudms_author_meta["last_name"][0]; ?>, <?php echo $edudms_author_meta["first_name"][0]; ?></a>
			</div>
			<div class="pt-email">
				<?php echo $edudms_author_meta["edudms_office"][0]; ?>
			</div>
			<div class="pt-phone">
				<?php echo $edudms_author_meta["edudms_phone"][0]; ?>
			</div>
			<div class="pt-office">
				<?php echo $edudms_author_meta["edudms_office"][0]; ?>
			</div>
		</div>
		<div class="pt-title-block">
			<?php echo $edudms_author_meta["edudms_title"][0]; ?>
		</div>
	</div>
	
	
	
	
		<?php //print_r($edudms_author_meta); ?>
		<?php //echo $user->ID; ?>


	<?php

}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

















?>