<?php







/**** Saving Functions ****/

function edudms_pt_save_extra_profile_fields( $user_id ) {


}


function edudms_pt_generate_edudms_field_list() {
	$all_pt_fields = get_posts([
			'post_type' => 'pt_field',
			'post_status' => 'publish',
			'numberposts' => -1
			// 'order'    => 'ASC'
		]);
	
		
		$edudms_pt_fields_arrayed = array();
		foreach($all_pt_fields as $field) {
			$fields_fields = get_fields($field->ID);
			array_push($edudms_pt_fields_arrayed,
				array(
						'edudms_field_option_show_feature'		=>	$fields_fields["edudms_field_option_show_feature"],
						'edudms_field_option_field_type'		=>	$fields_fields["edudms_field_option_field_type"],
						'edudms_field_option_proper_name'		=>	$fields_fields["edudms_field_option_proper_name"],
						'edudms_field_ID'						=>	$field->ID,
						'edudms_field_option_slug'				=>	$fields_fields["edudms_field_option_slug"] ));
		
		
		}
		return $edudms_pt_fields_arrayed;
}


function edudms_generate_acf_field_array() {
	
	
	$edudms_field_list = edudms_pt_generate_edudms_field_list();
	
	$the_return = array();
		foreach($edudms_field_list as $tab) {
			array_push($the_return, array(
				'key' => 'field_edudms_enable_' . $tab["edudms_field_option_slug"],
				'label' => 'Enable ' . $tab["edudms_field_option_proper_name"],
				'name' => 'edudms_enable_' . $tab["edudms_field_option_slug"],
				'type' => 'checkbox',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'Enable ' . $tab["edudms_field_option_proper_name"] . ' Tab' => 'Enable ' . $tab["edudms_field_option_proper_name"] . ' Tab',
				),
				'allow_custom' => 0,
				'default_value' => array(
				),
				'layout' => 'vertical',
				'toggle' => 0,
				'return_format' => 'value',
				'save_custom' => 0,
			));
			array_push($the_return, array(
				'key' => 'field_the_tab_' . $tab["edudms_field_option_slug"],
				'label' => $tab["edudms_field_option_proper_name"],
				'name' => 'edudms_tab_' . $tab["edudms_field_option_slug"],
				'type' => 'wysiwyg',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array(
					array(
						array(
							'field' => 'field_edudms_enable_' . $tab["edudms_field_option_slug"],
							'operator' => '==',
							'value' => 'Enable ' . $tab["edudms_field_option_proper_name"] . ' Tab',
						),
					),
				),
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
				'delay' => 0,
			));
		}
		
	return $the_return;
}


//use the above function to spit back the array ot insert into the following acf function
$edudms_the_fields_array = edudms_generate_acf_field_array();


//Create the tabs on peoples' profile editors
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5ced6852465f2',
	'title' => 'EduDMS Tabs',
	'fields' => $edudms_the_fields_array,
	'location' => array(
		array(
			array(
				'param' => 'user_form',
				'operator' => '==',
				'value' => 'edit',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;


































// Register Custom Post Type
function fields_post_type() {

	$labels = array(
		'name'                  => _x( 'PT Fields', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'PT Field', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Fields', 'text_domain' ),
		'name_admin_bar'        => __( 'Field', 'text_domain' ),
		'archives'              => __( 'Field Archives', 'text_domain' ),
		'parent_field_colon'     => __( 'Parent Field:', 'text_domain' ),
		'all_fields'             => __( 'All Fields', 'text_domain' ),
		'add_new_field'          => __( 'Add New Field', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_field'              => __( 'New Field', 'text_domain' ),
		'edit_field'             => __( 'Edit Field', 'text_domain' ),
		'update_field'           => __( 'Update Field', 'text_domain' ),
		'view_field'             => __( 'View Field', 'text_domain' ),
		'search_fields'          => __( 'Search Field', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_field'      => __( 'Insert into field', 'text_domain' ),
		'uploaded_to_this_field' => __( 'Uploaded to this field', 'text_domain' ),
		'fields_list'            => __( 'Fields list', 'text_domain' ),
		'fields_list_navigation' => __( 'Fields list navigation', 'text_domain' ),
		'filter_fields_list'     => __( 'Filter fields list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Field', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'page-attributes', 'revisions' ),
		'taxonomies'            => array( ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => 'edudms_ptslug',
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'edudms_cap',
	);
	register_post_type( 'pt_field', $args );

}
add_action( 'init', 'fields_post_type', 0 );



// Register Field Type Taxonomy
function field_type() {

	$labels = array(
		'name'                       		=> _x( 'Field Types', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              		=> _x( 'Field Type', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  		=> __( 'Taxonomy', 'text_domain' ),
		'all_field types'                  => __( 'All Field Types', 'text_domain' ),
		'parent_field type'                => __( 'Parent Field Type', 'text_domain' ),
		'parent_field type_colon'          => __( 'Parent Field Type:', 'text_domain' ),
		'new_field type_name'              => __( 'New Field Type Name', 'text_domain' ),
		'add_new_field type'               => __( 'Add New Field Type', 'text_domain' ),
		'edit_field type'                  => __( 'Edit Field Type', 'text_domain' ),
		'update_field type'                => __( 'Update Field Type', 'text_domain' ),
		'view_field type'                  => __( 'View Field Type', 'text_domain' ),
		'separate_field types_with_commas' => __( 'Separate field types with commas', 'text_domain' ),
		'add_or_remove_field types'        => __( 'Add or remove field types', 'text_domain' ),
		'choose_from_most_used'      		=> __( 'Choose from the most used', 'text_domain' ),
		'popular_field types'              => __( 'Popular Field Types', 'text_domain' ),
		'search_field types'               => __( 'Search Field Types', 'text_domain' ),
		'not_found'                  		=> __( 'Not Found', 'text_domain' ),
		'no_terms'                   		=> __( 'No field types', 'text_domain' ),
		'field types_list'                 => __( 'Field Types list', 'text_domain' ),
		'field types_list_navigation'      => __( 'Field Types list navigation', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'pt_field_type', array( 'pt_field' ), $args );
	
//Populate taxonomy with terms
	wp_insert_term( 'Tab', 'pt_field_type' , array( 'slug' => 'tab' ) );
	wp_insert_term( 'Shortfield', 'pt_field_type' );
	wp_insert_term( 'Dropdown', 'pt_field_type' );
	wp_insert_term( 'Checkbox', 'pt_field_type' );
	
}
add_action( 'init', 'field_type', 0 );


//Metabox for taxonomy
add_action( 'do_meta_boxes', 'metabox_field_type' );

function metabox_field_type() {
	remove_meta_box('slugdiv', 'field', 'normal' );
	remove_meta_box('authordiv', 'field', 'normal' );
	remove_meta_box('pageparentdiv', 'field', 'side' );
	remove_meta_box('tagsdiv-post_tag', 'field', 'side' );
	remove_meta_box('categorydiv', 'field', 'side' );
	remove_meta_box('revisionsdiv', 'field', 'side' );
	//remove_meta_box('field_typediv', 'field', 'side' );
	//add_meta_box('field_typediv',
	
}









/******************** PT FIeld Options ACF Group ***************/


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_edudms_pt_field_options_group',
	'title' => 'PT Fields Options',
	'fields' => array(
		array(
			'key' => 'field_option_show_feature',
			'label' => 'Show Feature',
			'name' => 'edudms_field_option_show_feature',
			'type' => 'select',
			'instructions' => '',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'enable' => 'Yes, this field should only show if a Person enables it.',
				'show_all' => 'No, this field should show on every Person\'s profile editor.',
				'single' => 'No, this field should show on only 1 Person\'s profile editor (not available yet).',
			),
			'default_value' => array(
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_option_field_type',
			'label' => 'Field Type',
			'name' => 'edudms_field_option_field_type',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'tab' => 'Tab (Wysiwyg, Full Editor)',
				'text' => 'Text Box (Single line text input)',
				'checkbox' => 'Checkbox (Future Feature)',
				'dropdown' => 'Dropdown (Future Feature)',
			),
			'default_value' => array(
				0 => 'tab',
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_option_proper_name',
			'label' => 'Proper Name',
			'name' => 'edudms_field_option_proper_name',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_option_slug',
			'label' => 'Slug',
			'name' => 'edudms_field_option_slug',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'pt_field',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;







































		
		
?>