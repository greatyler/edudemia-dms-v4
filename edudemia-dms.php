<?php
/*
Plugin Name: Edudemia Department Management System
Plugin URI: https://bitbucket.org/greatyler/edudemia-dms-v4
Bitbucket Plugin URI: greatyler/edudemia-dms-v4
Description: Create a Department Management System
Version: 4.3.1
people_tools_branch Version: 0.0.17
Author: Tyler Pruitt
Author URI:

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:




License

Edudemia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Edudemia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/


/**** Includes ****/
include_once dirname(__FILE__) . '/dms-cpt.php';
include_once dirname(__FILE__) . '/dms-cpt-test.php';
include_once dirname(__FILE__) . '/pt_core.php';
//include_once dirname(__FILE__) . '/acf-field_selector/acf-field_selector.php';
//include_once dirname(__FILE__) . '/acf-group_selector/acf-group_selector.php';
include_once dirname(__FILE__) . '/acf-post-type-selector/acf-post-type-selector.php';
//display engine v4
include_once dirname(__FILE__) . '/dms-display_engine.php';





/**** tgmpa Plugin Manager ****/
include_once dirname(__FILE__) . '/tgmpa-plugin/plugin_functions.php';
include_once dirname(__FILE__) . '/plugins.php';
require_once dirname(__FILE__) . '/tgmpa-plugin/tgmpa-config.php';
include_once dirname(__FILE__) . '/tgmpa-plugin/edudms_plugins.php';





function edudms_enqueue_admin_css() {
	wp_register_style( 'edudms_admin_css', plugins_url('css/edudms_admin_css.css',__FILE__ ));
    wp_enqueue_style( 'edudms_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'edudms_enqueue_admin_css' );

function edudms_css_registration() {
	wp_register_style('edudms', plugins_url('css/edudms.css',__FILE__ ));
	wp_enqueue_style('edudms');
}
add_action( 'wp_enqueue_scripts','edudms_css_registration');


//edudms_make_template('profile_page.php', 'Profile Page', 'edudemia-dms');









 
function edudms_install() {
    
   
   $faculty_member_type = array(
								'post_title' 	=>	"faculty",
								'post_status' 	=>	"publish",
								'post_type'		=>	"member_type" );
			wp_insert_post( $faculty_member_type );
	$staff_member_type = array(
								'post_title' 	=>	"staff",
								'post_status' 	=>	"publish",
								'post_type'		=>	"member_type" );
			wp_insert_post( $staff_member_type );
    $student_member_type = array(
								'post_title' 	=>	"student",
								'post_status' 	=>	"publish",
								'post_type'		=>	"member_type" );
			wp_insert_post( $student_member_type );
	$hidden_member_type = array(
								'post_title' 	=>	"hidden",
								'post_status' 	=>	"publish",
								'post_type'		=>	"member_type" );
			wp_insert_post( $hidden_member_type );
	
	
}
register_activation_hook( __FILE__, 'edudms_install' );




?>