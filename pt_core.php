<?php


include_once dirname(__FILE__) . '/pt/core-people_fields.php';



//Removed a bunch of stuff from the edit user interface.
include_once dirname(__FILE__) . '/pt/core-clean_interface.php';
include_once dirname(__FILE__) . '/pt/core-roles.php';
include_once dirname(__FILE__) . '/pt/shortcode-people_list.php';
include_once dirname(__FILE__) . '/pt/menu_page.php';
include_once dirname(__FILE__) . '/pt/member_types.php';
include_once dirname(__FILE__) . '/pt/core-contact_fields.php';
//locate_template(array(dirname(__FILE__) . '/pt/author.php'),false);








// Menu Options

function change_users_to_people() {
	global $menu;
	global $submenu;
	$menu[70][0] = 'People';
	$submenu['users.php'][5][0] = 'All People';
    $submenu['users.php'][10][0] = 'Add Person';
}

add_action( 'admin_menu', 'change_users_to_people' );



//Add Main Admin Menu Page
add_action ( 'admin_menu', 'edudms_pt_menu_page');
function edudms_pt_menu_page() {
		$edudms_pt_menu_page = add_menu_page ( 'People Tools', 'People Tools', 'manage_options', 'edudms_ptslug', 'edudms_pt_menu_page_render', plugin_dir_url( __FILE__ ) . 'images/icon_cream.png' );
		add_action( 'load-' . $edudms_pt_menu_page, 'load_edudms_ptcss_in_admin' );
		$edudms_pt_people_settings_subpage = add_users_page ( 'Edudemia People Tools', 'Settings', 'edudms_cap', 'edudms_pt_people_settings_subpage_slug', 'edudms_pt_menu_page_render' );
	}
	




//inject author template
function edudms_pt_author_template( $template )
{	
    if( is_author() ) {
        $template = dirname(__FILE__) . '/pt/author.php';
	}
	

    return $template;
}
add_filter( 'template_include', 'edudms_pt_author_template' );




//alter permalink base for authors
function edudms_pt_change_author_permalinks()  
{  
    global $wp_rewrite;  
    $wp_rewrite->author_base = 'people';  
    $wp_rewrite->author_structure = '/' . $wp_rewrite->author_base. '/%author%';  
}  
add_action('init','edudms_pt_change_author_permalinks'); 

































































?>