<?php

// Register Custom Post Type
function edudms_de_layout_post_type() {

	$labels = array(
		'name'                  => _x( 'DE_Layouts', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'DE_Layout', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'EduDMS Shortcodes', 'text_domain' ),
		'name_admin_bar'        => __( 'EduDMS Shortcode', 'text_domain' ),
		'archives'              => __( 'EduDMS Shortcode Archives', 'text_domain' ),
		'attributes'            => __( 'EduDMS Shortcode Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Shortcode:', 'text_domain' ),
		'all_items'             => __( 'All EduDMS Shortcodes', 'text_domain' ),
		'add_new_item'          => __( 'Add New Shortcode', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New EduDMS Shortcode', 'text_domain' ),
		'edit_item'             => __( 'Edit Shortcode', 'text_domain' ),
		'update_item'           => __( 'Update Shortcode', 'text_domain' ),
		'view_item'             => __( 'View EduDMS Shortcode', 'text_domain' ),
		'view_items'            => __( 'View EduDMS Shortcodes', 'text_domain' ),
		'search_items'          => __( 'Search EduDMS Shortcodes', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into Shortcode', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Shortcode', 'text_domain' ),
		'items_list'            => __( 'EduDMS Shortcodes list', 'text_domain' ),
		'items_list_navigation' => __( 'EduDMS Shortcodes list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter EduDMS Shortcodes list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'DE_Layout', 'text_domain' ),
		'description'           => __( 'layouts for the edudms layout feature of the display engine', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'de_layout', $args );

}
add_action( 'init', 'edudms_de_layout_post_type', 0 );

?>