<?php
/*
This core piece of dms will create post types for the other parts of edudemia. such as making news, newsletters, etc.
It will also allow for modules like publications to create books, journal articles, publications, and eventually allow users to create their own type of publication types.

*/


include_once dirname(__FILE__) . '/cpt/dms_cpt-content_types.php';
include_once dirname(__FILE__) . '/cpt/dms_cpt-menu_page.php';
include_once dirname(__FILE__) . '/cpt/dms_cpt-shortcode.php';



class edudms_cpt
{
    public $post_type_name;
	public $post_type_plural;
	public $post_type_slug;
    public $post_type_args;
    public $post_type_labels;
     
    /* Class constructor */
    public function __construct( $slug, $name, $plural, $base, $args = array(), $labels = array() )
	{
		
		
		
		
    // Set some important variables
	$this->post_type_slug		= str_replace( ' ', '_', $slug );
	$this->post_type_name		= $name;
	$this->post_type_plural		= $plural;
	$this->post_type_labels		= $labels;
    
	$this->post_type_args		= $args;
	
	if(empty($base)) {$this->post_type_args		= $args; }
	if($base == "book") { $this->post_type_args = array(
																'supports'		=> array( 'title', 'thumbnail' )
	);}
	if($base == "news") { $this->post_type_args = array(
																'supports'		=> array( 'title', 'editor', 'revisions', 'author' )
	);}
	if($base == "newsletter") { $this->post_type_args = array(
																'supports'		=> array( 'title' )
	);}
	if($base == "publication") { $this->post_type_args = array(
																'supports'		=> array( 'title' )
	);}
	
    // Add action to register the post type, if the post type does not already exist
    if( ! post_type_exists( $this->post_type_slug ) )
    {
        add_action( 'init', array( &$this, 'register_post_type' ) );
    }
     
    // Listen for the save post hook
    $this->save();
	}
     
    /* Method which registers the post type */
    public function register_post_type()
	{
		// make it plural
		$name       = $this->post_type_name;
		if(empty($this->post_type_plural)) { $plural = $name . 's'; } else { $plural = $this->post_type_plural; }
		
		 
		// We set the default labels based on the post type name and plural. We overwrite them with the given labels.
		$labels = array_merge(
		 
			// Default
			array(
				'name'                  => _x( $plural, 'post type general name' ),
				'singular_name'         => _x( $name, 'post type singular name' ),
				'add_new'               => _x( 'Add New', strtolower( $name ) ),
				'add_new_item'          => __( 'Add New ' . $name ),
				'edit_item'             => __( 'Edit ' . $name ),
				'new_item'              => __( 'New ' . $name ),
				'all_items'             => __( 'All ' . $plural ),
				'view_item'             => __( 'View ' . $name ),
				'search_items'          => __( 'Search ' . $plural ),
				'not_found'             => __( 'No ' . strtolower( $plural ) . ' found'),
				'not_found_in_trash'    => __( 'No ' . strtolower( $plural ) . ' found in Trash'), 
				'parent_item_colon'     => '',
				'menu_name'             => $plural
			),
			 
			// Given labels
			$this->post_type_labels
			 
		);
		 
		// Same principle as the labels. We set some defaults and overwrite them with the given arguments.
		$args = array_merge(
		 
			// Default
			array(
				'label'                 => $plural,
				'labels'                => $labels,
				'public'                => true,
				'show_ui'               => true,
				//'supports'              => array( 'title', 'editor' ),
				'show_in_nav_menus'     => true,
				'_builtin'              => false,
				'capability_type'			=> 'edudms_cap'
			),
			 
			// Given args
			$this->post_type_args
			 
		);
		 
		// Register the post type
		register_post_type( $this->post_type_slug, $args );
	}
     
    /* Method to attach the taxonomy to the post type */
    public function add_taxonomy( $slug, $args = array(), $labels = array() )
	{
		if( ! empty( $slug ) )
		{
			// We need to know the post type slug, so the new taxonomy can be attached to it.
			$post_type_slug = $this->post_type_slug;
	 
			// Taxonomy properties
			$taxonomy_slug      = $slug;
			$taxonomy_labels    = $labels;
			$taxonomy_args      = $args;
	 
			if( ! taxonomy_exists( $taxonomy_name ) ) {
				/* Create taxonomy and attach it to the object type (post type) */
			}
			else {
				/* The taxonomy already exists. We are going to attach the existing taxonomy to the object type (post type) */
				add_action( 'init',
						function() use( $taxonomy_slug, $post_type_slug )
						{
							register_taxonomy_for_object_type( $taxonomy_slug, $post_type_slug );
						}
					);
			}
		}
		}
		 
		/* Attaches meta boxes to the post type */
		public function add_meta_box()
    {
         
    }
     
    /* Listens for when the post type being saved */
    public function save()
    {
         
    }
}




function edudms_carry_out_cpt_creation() {
	$edudms_all_content_type = edudms_pt_generate_edudms_cpt_list();
	foreach($edudms_all_content_type as $edudms_type) {
		$the_type = new edudms_cpt( $edudms_type["edudms_cpt_slug"], $edudms_type["edudms_cpt_proper_name"], $edudms_type["edudms_cpt_special_plural"], $edudms_type["edudms_cpt_option_base_template"]);
	}
	
}

edudms_carry_out_cpt_creation();






function edudms_make_cpt_array( $post_type_slug = "book", $filter_by_field = "", $filter_by_value = "") {
	$edudms_cpt_content_array = array();
	
	//get the posts of the custom type
	$args = array(
					'numberposts' => -1,
					'post_type'   => 'book'
	);
	$edudms_cpt_content_posts = get_posts( $args );
	
	//make an array with the fields and the post content
	foreach($edudms_cpt_content_posts as $content_post) {
		$this_array = array(
				"fields" 		=>		get_fields( $content_post->ID ),
				"post"			=>		$content_post
		);
		
		$edudms_cpt_content_array[$content_post->ID] = $this_array;
		
	}
	
	//$edudms_cpt_content_array
	
	return $edudms_cpt_content_array;
}















?>